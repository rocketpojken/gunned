import React, { Component } from 'react';
import App from './App';
import marker1 from './Markers/T98C.jpg';
import marker2 from './Markers/Gog_enemy.jpg';
import marker3 from './Markers/shockerwhite-xls.png';
import hopper1 from './Hoppers/Gravity.jpg';
import hopper2 from './Hoppers/HalooHopper.jpg';
import hopper3 from './Hoppers/dye-Rotor.jpeg';
import goggle1 from './Goggles/Vforce-Grill-Front.jpg';
import goggle2 from './Goggles/dye-I4-Side.jpg';
import goggle3 from './Goggles/Dye-i5-Side.jpg';
import tank1 from './Tanks/Hpa-alu.jpg';
import tank2 from './Tanks/Safer-15L.png';
import tank3 from './Tanks/Dye-air-12L.jpg';
import Trigger from 'rc-trigger';
import 'rc-trigger/assets/index.css';
import Equipment from './Equipment.js';
{/*import Carthandle from './Carthandle'*/}


class Parts extends Component {

state = {
  chosenMarker: undefined,
  chosenHopper: undefined,
  chosenGoggles: undefined,
  chosenTanks: undefined,
  priceTanks: undefined,
  priceMarker: undefined,
  priceHopper: undefined,
  priceGoggles: undefined,
  picMarker: undefined,
  picHopper: undefined,
  picGoggles: undefined,
  picTanks: undefined,
  HoveredOnImg: false,
};


  handleClick = index => () => {
    if (index.type === 'marker') {
      this.setState({chosenMarker: index.name});
      this.setState({priceMarker: index.unitCost});
      this.setState({picMarker: index.pxc});

    }
    if (index.type === 'hopper') {
      this.setState({chosenHopper: index.name});
      this.setState({priceHopper: index.unitCost});
      this.setState({picHopper: index.pxc});

    }
    if (index.type === 'goggles') {
      this.setState({chosenGoggles: index.name});
      this.setState({priceGoggles: index.unitCost});
      this.setState({picGoggles: index.pxc});

    }
    if (index.type === 'tanks') {
      this.setState({chosenTanks: index.name});
      this.setState({priceTanks: index.unitCost});
      this.setState({picTanks: index.pxc});
    }
  };




render() {

  const data = {
    Guns: [
        {name: 'Tippmann 98', unitCost: 1200, wight: 2350, desc: 'T98', pxc: marker1, gtype: 'Newbie', color_1: '#000000', color_2: 'none'},
        {name: 'Gog Enemny', unitCost: 1749, wight: 1626, desc: 'Gog', pxc: marker2, gtype: 'Advc', color_1: '#2e4baa', color_2: 'none'},
        {name: 'Shocker XLS', unitCost: 3699, wight: 1468, desc: 'Shocker', pxc: marker3, gtype: 'Pro', color_1: '#912150', color_2: 'none'},
      ],
    Hoppers:[
        {name: 'Gravity', unitCost: 99, wight: 60, desc: 'Standard gravitations magasin', pxc: hopper1, gtype: 'Newbie', color_1: '#000000', color_2: 'none'},
        {name: 'Haloo', unitCost: 490, wight: 645, desc: 'Ett av dem första eldrivna magasinen', pxc: hopper2, gtype: 'Advc', color_1: '#f2f6fc', color_2: 'none'},
        {name: 'Rotor', unitCost: 899, wight: 780, desc: 'Det mest sålda elmagasinet', pxc: hopper3, gtype: 'Pro', color_1: '#000000', color_2: '#c2cecd'},

      ],
    Goggles:[
        {name: 'V-force Grill', unitCost: 649, wight: 180, desc: 'Mr StormTrooopers', pxc: goggle1, gtype: 'Advc', color_1: '#000000', color_2: '#FFFFFF'},
        {name: 'Dye I4', unitCost: 749, wight: 193, desc: 'Mest sålda masken på marknaden', pxc: goggle2, gtype: 'Semi-Pro', color_1: '#d9e2e2', color_2: '#e51919'},
        {name: 'Dye I5', unitCost: 1149, wight: 182, desc: 'Dye nyaste mask! I5 tar marknaden med storm', pxc: goggle3, gtype: 'Pro', color_1: '#e51919', color_2: '#1848e5'},
      ],
    Tanks:[
        {name: 'Alu', unitCost: 435, wight: 1850, desc: 'Standard aluminium tank trycktid 10år', pxc: tank1, gtype: 'Newbie'},
        {name: 'Safer 1.5L', unitCost: 1495, wight: 580, desc: 'Flaska som klarar 4500psi väger bara 580gram. Trycktid 5år', pxc: tank2, gtype: 'Advc'},
        {name: 'Dye UL 1.2L', unitCost: 1695, wight: 565, desc: 'Klarar 4500psi 5års Trycktid', pxc: tank3, gtype: 'Advc'},
      ],
};

  const { chosenMarker,
    priceMarker, chosenHopper, priceHopper, chosenGoggles, priceGoggles, chosenTanks, priceTanks, picMarker, picHopper, picGoggles, picTanks,
  HoveredOnImg
} = this.state;
{/* fullpack funkar inte pga av att map inte är genom för här? */}

  {/* Ja den är lite knepig. Du kan inte hitta nyckeln pxc sådär. data.Guns är rätt men du kan inte skriva ut dem.
   Du måste iterera igenom Guns för att få fram varje pxc om du inte använder en funktion som tex find.

   Titta på mappningen av guns
    {data.Guns.map(({ name, desc, gtype, unitCost }) => {

    Först tar vi data, sedan tar vi nyckeln Guns, går in i den och plockar ut alla delar från objektet.

   */}
   console.log(picMarker, picTanks, picGoggles, picHopper)
const fullPack = <div id="korg">
    <div id="upperCase">
<img src={this.state.picMarker} alt="Marker"/>

<img src={this.state.picHopper} alt="Hopper"/>
    </div>

    <div id="lowerCase">
<img src={this.state.picGoggles} alt="Goggles"/>


<img src={this.state.picTanks} alt="Tank"/>
    </div>
</div>;


  const finalSum =  +priceMarker + +priceHopper + +priceGoggles + +priceTanks;
  const printSum = <p>Total price is: {finalSum}</p>;
  const cartNext = <div>
    <form>
      <button>Go to Checkout</button>
    </form>
  </div>;



{/*
render() {
  const cartReady = this.state.cartReady;
  return(
    <div>
      {cartReady ? '<button to="checkout">Checkout next</button>' : 'You are missing a/or few things in the cart!'}
    </div>
  );
}

*/}

{/*  if('chosenMarker' || 'chosenHopper' || 'chosenGoggles' || 'chosenTanks' === index){
  Checkout = <button to="Checkout">Cart</button>;
  }else {
    Checkout = "Du har inte valt en sak från varje kategori";
  }
};
*/}


  return (



    <div id="wraper">

      <h2>Markers</h2>
      <hr />
    {data.Guns.map(({ name, desc, gtype, unitCost, pxc }, index) => {
      return (
<div
  onClick={this.handleClick({name, type: 'marker', unitCost, pxc})}
>
<Equipment
  index={index}
  name={name}
  desc={desc}
  gtype={gtype}
  unitCost={unitCost}
  pxc={pxc}

   />
</div>


      );
    })}
            <hr />
            <h2>Magasin</h2>
            <hr />
      {data.Hoppers.map(({ name, desc, gtype , unitCost, pxc }, index) => {
        return (

          <div
            onClick={this.handleClick({name, type: 'hopper', unitCost, pxc})}
          >
          <Equipment
            index={index}
            name={name}
            desc={desc}
            gtype={gtype}
            unitCost={unitCost}
            pxc={pxc}

             />
          </div>
        );
      })}
        <hr />
          <h2>Masker</h2>
          <hr />
      {data.Goggles.map(({ name, desc, gtype, unitCost, pxc  }, index) => {
        return(

          <div
            onClick={this.handleClick({name, type: 'goggles', unitCost, pxc})}
            >
            <Equipment
              index={index}
              name={name}
              desc={desc}
              gtype={gtype}
              unitCost={unitCost}
              pxc={pxc}

               />
          </div>
        );
      })}
          <hr />
          <h2>Tankar</h2>
          <hr />
      {data.Tanks.map(({ name, desc, gtype, unitCost, pxc }, index) => {
        return(

          <div
            onClick={this.handleClick({name, type: 'tanks', unitCost, pxc})}
            >

            <Equipment
              index={index}
              name={name}
              desc={desc}
              gtype={gtype}
              unitCost={unitCost}
              pxc={pxc}

               />
          </div>
        );
      })}

      {picGoggles && picHopper && picMarker && picTanks && (<hr />)}
      <br />

      {picGoggles && picHopper && picMarker && picTanks && (fullPack)}
      <br />
      {chosenMarker} , {chosenHopper} , {chosenGoggles} , {chosenTanks}
      {priceMarker && priceHopper && priceGoggles && priceTanks && (printSum)}
      {chosenGoggles && chosenHopper && chosenMarker && chosenTanks && (cartNext)}
      {/*   vad betyder ()? - () skaparen en return */}
    </div>


);

    }
  }

export default Parts;
