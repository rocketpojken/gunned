import React, { Component } from 'react';

class Equipment extends Component {

  state = {
    HoveredOnImg: false,
  };
  setImgHovered = inputData => () => {
    this.setState({HoveredOnImg: inputData});

  }



  render(){

    const {index, name, desc, gtype, unitCost, pxc } = this.props;
    const {HoveredOnImg} = this.state;

    return(
      <div
        key={index}
        onMouseEnter={this.setImgHovered(index)}
        onMouseLeave={this.setImgHovered(undefined)}
        className={HoveredOnImg === index ? 'hover' : null}
      >
        <img
          src={pxc}
          alt={desc}
           /> <br />
           {HoveredOnImg === index ? desc : null}

      </div>
    );
  }
}

export default Equipment;
